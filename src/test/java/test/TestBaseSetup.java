package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBaseSetup {

    public static WebDriver driver = null;

    public WebDriver initializeDriver() throws IOException {
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("D:\\Automation\\Projects\\phptravels\\src\\test\\resources\\config\\env.properties");
        properties.load(fileInputStream);
        if (properties.getProperty("browser").equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Projects\\phptravels\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get(properties.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } else if (properties.getProperty("browser").equals("IE")) {
            //launch Internet explorer
            driver = new InternetExplorerDriver();
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get(properties.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } else if (properties.getProperty("browser").equals("Firefox")) {
            //launch Firefox
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get(properties.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } else {
            //default
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get(properties.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }

        return driver;
    }

    public void closeBrowser() throws Exception {
        TestBaseSetup.driver.quit();
    }
}