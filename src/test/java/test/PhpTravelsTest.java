package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pages.PhpTravelsPage;

public class PhpTravelsTest extends TestBaseSetup {


    @AfterMethod
    public void tearDown() throws Exception {
        closeBrowser();
    }


    @Test(description = "validate all objects on PHPTravels Page")
    public void valdiateAllObjectsOnPhPTravelsPageAreDisplayedCorrectly() throws Exception {
        driver = initializeDriver();
        PhpTravelsPage phpTravelsPage = new PhpTravelsPage(driver);

        phpTravelsPage.validateUserNavigatedToPhpTravelsPage();
    }


}
