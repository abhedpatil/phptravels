package test;

import org.testng.annotations.Test;
import pages.FeaturesHotelsPage;

public class FeaturesTest extends TestBaseSetup {


    @Test(description = "User clicks on Hotels from Features dropdown")
    public void validateUserNavigatedToHotelsFeaturesPage() throws Exception {
        driver = initializeDriver();
        FeaturesHotelsPage featuresHotelsPage = new FeaturesHotelsPage(driver);

        featuresHotelsPage.validateUserNavigatedToHotelsPage();
    }
}
