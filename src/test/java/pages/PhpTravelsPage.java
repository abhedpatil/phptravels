package pages;

import constants.PhpTravelsConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.AbstractPage;

public class PhpTravelsPage extends AbstractPage implements PhpTravelsConstants {

    private static Logger log = LogManager.getLogger(PhpTravelsPage.class.getName());

    public WebDriver driver = null;

    public PhpTravelsPage(WebDriver driver) {
        super(driver);
        //this.driver = driver;
        //PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/header/div[2]/div/a/img")
    WebElement phpTravelsLogo;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[1]/span/span/a")
    WebElement demo;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement pricing;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/span")
    WebElement features;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[4]/span/span/a")
    WebElement product;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[5]/span/span/a")
    WebElement hosting;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[6]/span/span/a")
    WebElement services;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[7]/span/span/a")
    WebElement company;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement blog;

    @FindBy(id = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement login;

    public void validateUserNavigatedToPhpTravelsPage() {
        verifyObjectIsDisplayed(phpTravelsLogo);
        verifyObjectIsDisplayed(demo);
        verifyObjectContainsText(demo, PhpTravelsConstants.demo);
        verifyObjectIsDisplayed(pricing);
        verifyObjectContainsText(pricing, PhpTravelsConstants.pricing);
        verifyObjectIsDisplayed(features);
        verifyObjectContainsText(features, PhpTravelsConstants.features);
        //Assert.assertTrue(phpTravelsLogo.isDisplayed());
        //log.info(" phpTravelsLogo displayed");
        //Assert.assertTrue(demo.isDisplayed());
        //Assert.assertEquals(demo.getText(), "DEMO");  //providing input

        //Assert.assertTrue(demo.getText().contains(PhpTravelsConstants.demo));
        //Assert.assertTrue(pricing.isDisplayed());
        //Assert.assertEquals(pricing.getText(), PhpTravelsConstants.pricing);  //Calling from Interface
        //Assert.assertTrue(features.isDisplayed());
        //Assert.assertEquals(features.getText(), PhpTravelsConstants.features);
        //TODO: Add all other webelements and validaate
    }
}
