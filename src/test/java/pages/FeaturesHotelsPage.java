package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import setup.AbstractPage;

import java.util.concurrent.TimeUnit;

public class FeaturesHotelsPage extends AbstractPage {

    public WebDriver driver = null;

    public FeaturesHotelsPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/span/span")
    WebElement features;

    @FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/div/ul/li[2]/a")
    WebElement hotels;

    @FindBy(xpath = "/html/body/section[1]/div[1]/div/div/h2")
    WebElement hotelBookingText;

    public void selectHotelsFromFeaturesList() {

        Actions actions = new Actions(driver);
        actions.moveToElement(features).perform();
        hotels.click();
    }

    public void validateUserNavigatedToHotelsPage() throws Exception {
        selectHotelsFromFeaturesList();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String hotelBookingModuleFeature = hotelBookingText.getText();
        System.out.println(hotelBookingModuleFeature);
        Assert.assertTrue(hotelBookingModuleFeature.contains("HOTEL BOOKING MODULE FEATURES"));

    }
}
