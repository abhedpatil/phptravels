package setup;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class AbstractPage {

    private final static int millisecondsToSleep = 1000; //in milliseconds
    public WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static String getFormatedDate(String format) {
        Date date = new Date();
        return new SimpleDateFormat(format).format(date);
    }

    // Retrieves the current date as ISO 8601 string
    public static String getISOdate() {
        return getFormatedDate("yyyy-MM-dd");
    }

    // Retrieves the current time as ISO 8601 string
    public static String getISOtime() {
        return getFormatedDate("HH:mm:ss");
    }

    public void verifyObjectContainsText(WebElement element, String comparision) {
        String actualText = element.getText();
        if (actualText.contains(comparision)) {
            Assert.assertTrue(actualText.contains(comparision), "The value in the given element is " + actualText + " and expected is " + comparision);
        }
        ;
        log("VERIFICATION: Text '" + comparision + "' in element verified! (Element:'" + element + "')");
    }

    public void verifyObjectContainsExactText(WebElement element, String comparision) {
        String actualText = element.getText();
        if (actualText.equals(comparision)) {
            Assert.assertTrue(actualText.equals(comparision), "The value in the given element is " + actualText + " and expected is " + comparision);
        }
        ;
        log("VERIFICATION: Text '" + comparision + "' in element verified! (Element:'" + element + "')");
    }

    public void verifyObjectIsDisplayed(WebElement element) {
        boolean displayed = element.isDisplayed();
        Assert.assertTrue(displayed, "Element not displayed" + element);
        log("VERIFICATION: Element displayed as expected (Element:'" + element + "')");
    }

    public void sleep(int milliseconds) {
        try {
            if (milliseconds > 1000) {
                log("Waiting for " + (milliseconds / 1000) + " second(s) before next interaction...");
            }
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            System.out.println("Interrupt Exception Encountered.");
        }
    }

    public void sleep() {

        sleep(millisecondsToSleep);
    }

    public void log(String string) {

        String time = getFormatedDate(getISOdate() + " " + getISOtime());
        System.out.println("** " + time + " " + string);
    }
}
